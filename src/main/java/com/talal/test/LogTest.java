package com.talal.test;

import com.talal.logging.Logger;
import com.talal.logging.LoggerProvider;
import com.talal.logging.SimpleFileLogger;

public class LogTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ComponentDemo c1 = new ComponentDemo("ComponentDemo-1", 10);
		c1.start();
		ComponentDemo c2 = new ComponentDemo("ComponentDemo-2", 100);
		c2.start();
		ComponentDemo c3 = new ComponentDemo("ComponentDemo-3", 1000);
		c3.start();
	}
}

class ComponentDemo implements Runnable {
	private Thread t;
	private String threadName;
	private long mInterval;
	Logger logger;

	ComponentDemo(String name, long interval) {
		logger = LoggerProvider.getLogger(SimpleFileLogger.class);
		threadName = name;
		mInterval = interval;
		System.out.println("Creating " + threadName);
	}

	public void run() {
		System.out.println("Running " + threadName);
		try {
			for (int i = 1; i <= 10000; i++) {
				logger.log("Thread: This is a test " + i, threadName);
				// Let the thread sleep for a while.
				Thread.sleep(mInterval);
			}
		} catch (InterruptedException e) {
			System.out.println("Thread " + threadName + " interrupted.");
		}
		System.out.println("Thread " + threadName + " exiting.");
	}

	public void start() {
		System.out.println("Starting " + threadName);
		if (t == null) {
			t = new Thread(this, threadName);
			t.start();
		}
	}

}
