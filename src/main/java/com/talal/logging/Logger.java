package com.talal.logging;

public interface Logger {

	void log(final String message, final String className);
	
	void log(final LogTask logTask);
	
	void error(final String message, final String className);

}
