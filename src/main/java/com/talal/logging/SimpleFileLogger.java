package com.talal.logging;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import static java.util.concurrent.TimeUnit.*;

/**
 * @author talal
 * 
 */
public class SimpleFileLogger implements Logger {
	private String ERROR_TAG = "ERROR";
	private String DEFAULT_TAG = "INFO";
	private long FILE_CREATION_INTERVAL = MILLISECONDS.convert(15, MINUTES);
	private SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat(
			"dd/MM/YYYY hh:mm:ss");
	private SimpleDateFormat FILE_DATE_FORMAT = new SimpleDateFormat(
			"dd-MM-YYYY.hh-mm-ss");

	private Lock queueLock = new ReentrantLock();
	private Condition queueHasTasks = queueLock.newCondition();
	private Queue<LogTask> logTasks = new LinkedList<LogTask>();
	private String path;
	private String fileName;
	private Date lastTimeSuffix;

	private SimpleFileLoggerRunnable loggerRunnable;

	public SimpleFileLogger(final String path, final String fileName) {
		this.path = path;
		this.fileName = fileName;
		this.lastTimeSuffix = new Date();
		loggerRunnable = new SimpleFileLoggerRunnable();
		Thread worker = new Thread(loggerRunnable);
		worker.start();
	}

	public void log(final String message, final String className) {
		this.log(new LogTask(DEFAULT_TAG, message, className));
	}

	public void error(final String message, final String className) {
		this.log(new LogTask(ERROR_TAG, message, className));
	}

	public void log(final LogTask task) {
		if (!loggerRunnable.isRunning()) {
			System.out.println("Logger is not running");
			return;
		}
		queueLock.lock();
		logTasks.add(task);
		queueHasTasks.signal();
		queueLock.unlock();
	}

	public void stopLogging() {
		loggerRunnable.setRunning(false);
	}

	private class SimpleFileLoggerRunnable implements Runnable {
		private boolean isRunning = true;

		public void run() {
			while (true) {
				boolean isQueueUnlocked = false;
				queueLock.lock(); // locking only for getting task
				if (!isRunning) {
					if (logTasks.isEmpty()) {
						return; // exiting
					}
				}
				try {
					// Wait for new jobs
					while (logTasks.isEmpty()) {
						try {
							queueHasTasks.await();
							if (!isRunning) {
								if (logTasks.isEmpty()) {
									return; // exiting
								}
							}
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}

					while (!logTasks.isEmpty()) {
						LogTask task = logTasks.poll();
						queueLock.unlock();
						bufferedWriteLog(task);
						queueLock.lock();
					}
					queueLock.unlock();
					isQueueUnlocked = true;
				} finally {
					if (!isQueueUnlocked) {
						queueLock.unlock();
					}
				}
			}
		}

		public String getFileName() {
			Date currentTime = new Date();
			if ((currentTime.getTime() - lastTimeSuffix.getTime()) >= FILE_CREATION_INTERVAL) {
				lastTimeSuffix = currentTime;
			}
			return (FILE_DATE_FORMAT.format(lastTimeSuffix) + "-" + fileName);
		}

		public void setRunning(final boolean isRunning) {
			this.isRunning = isRunning;
		}

		public boolean isRunning() {
			return isRunning;
		}

		private String logTaskToString(final LogTask task) {
			String message = task.message;
			String tag = task.tag;
			Date d = task.date;
			String c = task.className;
			String out = "[" + DEFAULT_DATE_FORMAT.format(d) + "]" + "[" + c
					+ "][" + tag + "]: " + message + "\n";
			return out;
		}

		public void bufferedWriteLog(LogTask task) {
			Path fileP = Paths.get(path + getFileName());
			try {
				Files.createDirectories(fileP.getParent());
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			try (BufferedWriter writer = Files.newBufferedWriter(fileP,
					StandardCharsets.UTF_8, StandardOpenOption.CREATE,
					StandardOpenOption.APPEND)) {
				writer.write(logTaskToString(task));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}	

	}

}
