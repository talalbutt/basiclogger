package com.talal.logging;

import java.util.HashMap;

public final class LoggerProvider {

	private static HashMap<Class, Logger> loggers = new HashMap<>();

	public static Logger getLogger(Class clazz) {
		if (loggers.containsKey(clazz)) {
			return loggers.get(clazz);
		}

		if (clazz.equals(SimpleFileLogger.class)) {
			Logger logger = (new LoggerFactory()).getLogger(clazz);
			loggers.put(clazz, logger);
			return logger;
		}
		return null;
	}

}
