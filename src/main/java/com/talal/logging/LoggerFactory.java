package com.talal.logging;

public class LoggerFactory {
	public Logger getLogger(Class clazz) {
		if (clazz == null) {
			return null;
		}
		if (clazz.equals(SimpleFileLogger.class)) {
			String workingDir = System.getProperty("user.dir");
			return new SimpleFileLogger(workingDir + "/logs/", "test.log");
		}
		return null;
	}
}
