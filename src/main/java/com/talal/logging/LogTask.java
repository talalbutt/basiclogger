package com.talal.logging;

import java.util.Date;

public class LogTask {
	
	final String tag;
	final String message;
	final Date date;
	final String className;
	
	public LogTask(final String tag, final String message, final String className) {
		this.tag = tag;
		this.message = message;
		this.className = className;
		date = new Date();
	}

}
